#! /bin/python3
import requests
import datetime
import configparser
import time
from rich import print, pretty
import os

def getData(config: configparser.ConfigParser):
    stations = config['GENERAL']['stations'].split(',')
    nextStations = []
    for station in stations:
        url=f"https://www.vrn.de/mngvrn/XML_DM_REQUEST?depType=stopEvents&includeCompleteStopSeq=1&limit=5&locationServerActive=1&mode=direct&name_dm={station}&outputFormat=json&type_dm=any&useOnlyStops=1&useRealtime=1"
        r = requests.get(url, timeout=10)
        if r.status_code == 200:
            j = r.json()
            departurelist = j.get('departureList')
            if departurelist:
                for ele in departurelist:
                    if ele.get("realDateTime"):
                        time = ele.get("realDateTime")
                    else:
                        time = ele.get("dateTime")
                    formattedTime = datetime.datetime(int(time.get("year")), int(time.get("month")), int(time.get("day")), int(time.get("hour")), int(time.get("minute")))
                    nextStations.append({
                        "name": ele.get("nameWO"),
                        "to": ele.get("servingLine").get("direction"),
                        "time": formattedTime.strftime("%H:%M"),
                        "relTime": (formattedTime - datetime.datetime.now()).total_seconds() / 60,
                        "line": ele.get("servingLine").get("symbol"),
                    })
    nextStations = sorted(nextStations, key=lambda x: x["relTime"], reverse=True)
    
    return nextStations

def printData(data: []):
    if data:
        print("The next Depatures are:")
        for ele in data:
            print(f"{ele.get('line')} to [#FFFF00]{ele.get('to')}[/]")
            print(f"\t {ele.get('time')} ({ele.get('relTime'):.1f} min)")
    else:
        print("There are no departures in the near future or there is no Internet. Stay tuned!")
    print(f"The Time now is {datetime.datetime.now().strftime('%H:%M')}.")

def main():
    config = configparser.ConfigParser()
    config.read('app.config')
    pretty.install()
    while True:
        os.system('cls' if os.name == 'nt' else 'clear')
        try:
            data = getData(config)
            printData(data)
        except:
            pass
        time.sleep(60)

if __name__ == "__main__":
    main()