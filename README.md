# train proclaim

Gets the data from the next vrn station(s) and shows them on a terminal. Refreshes every x minutes.

## Usage

Change the app.config.example to app.config and change the station to your desired one. You can find the via checking the Console when Using the vrn Abfahrtsmonitor (https://www.vrn.de/index.html - look for the call 'XML_DM_REQUEST' and search in the response after 'dm') or by using the API by DB (https://developer.deutschebahn.com/store/apis/info?name=StaDa-Station_Data&version=v2&provider=DBOpenData - you need to sign up)
